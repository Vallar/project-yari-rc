﻿using UnityEngine;
using System.Collections;
using MongoDB.Driver;
using MongoDB.Bson;

[CreateAssetMenu(menuName = "Database/Database Config")]
public class DatabaseConfig : ScriptableObject
{
    public string dbURL = "localhost";
    public string databaseName = "Project5";
    public int port = 27017;
}
