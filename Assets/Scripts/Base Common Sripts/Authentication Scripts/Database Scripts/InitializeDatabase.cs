﻿using UnityEngine;
using System.Collections;
using MongoDB.Driver;
using MongoDB.Bson;

[CreateAssetMenu(menuName = "Database/Database Initialization")]
public class InitializeDatabase : ScriptableObject
{
    public MongoDatabase database;

    [SerializeField] private DatabaseConfig config;
    
    public void PerpareDatabase()
    {
        MongoServerSettings settings = new MongoServerSettings();
        settings.Server = new MongoServerAddress(config.dbURL, config.port);

        MongoServer server = new MongoServer(settings);
        database = server.GetDatabase(config.databaseName);
    }
}
