﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DatabaseManager : NetworkBehaviour {

    [SerializeField] private DatabaseConfig config;
    [SerializeField] private InitializeDatabase initDatabase;

    private void Awake()
    {
        if (isServer == false)
            return;
        else if (isServer)
            initDatabase.PerpareDatabase();
    }

}