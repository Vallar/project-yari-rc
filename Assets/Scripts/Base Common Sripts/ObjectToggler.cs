﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Object/Object Toggler")]
public class ObjectToggler : ScriptableObject
{
    [SerializeField] private GameObject[] activeObjectsArray;

    [SerializeField] private GameObject[] disabledObjectsArray;

    public void ToggleObjects()
    {
        for (int i = 0; i < activeObjectsArray.Length; i++)
        {
            if (activeObjectsArray[i].activeInHierarchy)
                activeObjectsArray[i].SetActive(false);
            else
                activeObjectsArray[i].SetActive(true);
        }

        for (int i = 0; i < disabledObjectsArray.Length; i++)
        {
            if (disabledObjectsArray[i].activeInHierarchy)
                disabledObjectsArray[i].SetActive(false);
            else
                disabledObjectsArray[i].SetActive(true);
        }
    }
}
