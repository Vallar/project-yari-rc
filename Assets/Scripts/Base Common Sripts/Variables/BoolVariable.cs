﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/Bool Variable")]
public class BoolVariable : ScriptableObject
{
    [HideInInspector] public bool value;

    [SerializeField] private bool originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
