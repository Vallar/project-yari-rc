﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/Byte Variable")]
public class ByteVariable : ScriptableObject
{
    [HideInInspector] public byte value;

    [SerializeField] private byte originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
