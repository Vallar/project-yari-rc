﻿using UnityEngine;
using System.Collections;
using TMPro;

[CreateAssetMenu(menuName = "Variables/GUIText Variable")]
public class GUITextVariable : ScriptableObject
{
    [HideInInspector] public TMP_InputField value;

    [SerializeField] private TMP_InputField originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
