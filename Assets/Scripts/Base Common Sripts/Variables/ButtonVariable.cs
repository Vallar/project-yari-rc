﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Variables/Button Variable")]
public class ButtonVariable : ScriptableObject
{
    [HideInInspector] public Button value;

    [SerializeField] private Button originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
