﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/Float Variable")]
public class FloatVariable : ScriptableObject
{
    [HideInInspector] public float value;

    [SerializeField] private float originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
