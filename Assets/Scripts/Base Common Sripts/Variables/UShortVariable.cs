﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/uShort Variable")]
public class UShortVariable : ScriptableObject
{
    [HideInInspector] public ushort value;

    [SerializeField] private ushort originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
