﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/Int Variable")]
public class IntVariable : ScriptableObject
{
    [HideInInspector] public int value;

    [SerializeField] private int originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
