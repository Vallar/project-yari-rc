﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/String Variable")]
public class StringVariable : ScriptableObject
{
    [HideInInspector] public string value;

    [SerializeField] private string originalValue;

    private void Awake()
    {
        value = originalValue;
    }
}
